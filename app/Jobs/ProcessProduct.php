<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\Product;

class ProcessProduct implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * 
     */



    public function __construct()
    {

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = resource_path('temp');
        $files = glob("$path/*.csv");
        
        $data = array_map(function($v){return str_getcsv($v, ";");}, file($files[0]));
        $header = $data[0];



        foreach($files as $key => $file){
            $data = array_map(function($v){return str_getcsv($v, ";");}, file($file));
            foreach($data as $key => $product){
                if($key !== 0){
                    $productData = array_combine($header,$product);
                    Product::create($productData);
                }

            }
            unlink($file);

        }
    }
}
