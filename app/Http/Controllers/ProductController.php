<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;
use App\Jobs\ProcessProduct;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $products = Product::all();
        return response()->json($products);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new Product();
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->ean13 = $request->ean13;
        $product->asin = $request->asin;
        $product->isbn = $request->isbn;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->save();
        return response()->json('Le produit a été ajouté avec succès.');
    }

    public function upload(Request $request)
    {
        $data = file(resource_path('csv/csv4.csv'));
        $chunks = array_chunk($data, 100);
        //$data = array_map(function($v){return str_getcsv($v, ";");}, file($filename));
        // $header = [];
        // unset ($data [0]);

        // Chunking file
        $chunks=array_chunk($data,100);
        foreach($chunks as $key => $chunk){
            $name = "/tmp{$key}.csv";
            $path = resource_path('temp');
            file_put_contents($path.$name,$chunk);

        }

     
        return response()->json('Le produit a été ajouté avec succès.');
        
    }

    public function jobStore(){
        ProcessProduct::dispatch();
        return 'stored';
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->name = $request->name;
        $product->sku = $request->sku;
        $product->ean13 = $request->ean13;
        $product->asin = $request->asin;
        $product->isbn = $request->isbn;
        $product->price = $request->price;
        $product->stock = $request->stock;
        $product->update();
        return response()->json('Le produit a été modifié avec succès.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Product::destroy($id);
        return response()->json('Le produit a été supprimé avec succès.');
    }
}
